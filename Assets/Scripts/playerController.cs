﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{

    #region Variables
    // public Vector3 finalLocation = new Vector3(-36f, 0.9866668f, 2f);
    public ScoreManager sm;
    public Vector3 finalLocation = new Vector3(-42.18f, 1.685f, 5.5f);
    public Camera _camera;
    public PostProcessingProfile cc, blur;
    // public Vector3 Target = new Vector3(-19, 0.5f, 5);
    Vector3 Target = new Vector3(-62, 1.5f, 36.5f);
    public NavMeshAgent agent;
    public GameObject[] MCQUI;
    float m_xAxis = 1.6412f;
    float m_yAxis = -50.597f;
    RectTransform m_rectTransform;
    GameObject mcq1_Clone, mcq2_Clone, mcq3_Clone, mcq4_Clone;
    public GameObject _MCQ1, _MCQ2, _MCQ3, _MCQ4, _MCQ5;
    int mcq_number = 0;
    public GameObject[] Collectables;
    // public GameObject 

    #endregion
    public void StartButton()
    {
        agent.SetDestination(Target);
        if (GameObject.FindGameObjectWithTag("MCQ") == null)
        {
            agent.speed = 10;
            agent.SetDestination(Target);
        }
    }
    public void goToHome()
    {
        SceneManager.LoadSceneAsync("HomeScreen");

    }

    void OnTriggerEnter(Collider col)
    {
        switch (col.gameObject.tag)
        {
            case "T1":
                agent.speed = 0;
                mcq_number = 1;
                MCQHandle();
                Collectables[0].SetActive(false);
                break;

            case "T2":
                agent.speed = 0;
                mcq_number = 2;
                MCQHandle();
                Collectables[1].SetActive(false);
                break;

            case "T3":
                agent.speed = 0;
                mcq_number = 3;
                MCQHandle();
                Collectables[2].SetActive(false);
                break;

            case "T4":
                agent.speed = 0;
                mcq_number = 4;
                MCQHandle();
                Collectables[3].SetActive(false);
                break;

            case "T5":
                agent.speed = 0;
                mcq_number = 5;
                MCQHandle();
                Collectables[4].SetActive(false);
                break;

            case "FinalDestination":
                sm.SetBoolFalse();
                _camera.GetComponent<PostProcessingBehaviour>().profile = blur;
                break;

            default:
                break;
        }
    }

    void MCQHandle()
    {
        switch (mcq_number)
        {
            case 1:
                _MCQ1.SetActive(true);
                break;

            case 2:
                _MCQ2.SetActive(true);
                break;

            case 3:
                _MCQ3.SetActive(true);
                break;

            case 4:
                _MCQ4.SetActive(true);
                break;

            case 5:
                _MCQ5.SetActive(true);
                break;

            default:
                break;
        }
    }
    public void resume()
    {
        agent.speed = 10;
        switch (mcq_number)
        {
            case 1:
                _MCQ1.SetActive(false);
                break;

            case 2:
                _MCQ2.SetActive(false);
                break;

            case 3:
                _MCQ3.SetActive(false);
                break;

            case 4:
                _MCQ4.SetActive(false);
                break;

            case 5:
                _MCQ5.SetActive(false);
                break;

            default:
                break;
        }
    }

    #region InstantiatingMCQS
    void handleMCQ1()
    {
        mcq1_Clone = Instantiate(MCQUI[0], transform.position, transform.rotation) as GameObject;
        mcq1_Clone.transform.SetParent(GameObject.FindGameObjectWithTag("UI").transform, false);
        mcq1_Clone.transform.rotation = Quaternion.Euler(0, 0, 0);
        mcq1_Clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_xAxis, m_yAxis);
        mcq1_Clone.tag = "MCQ";
        // mcq1_Clone.transform.position = new Vector3(1.6412f, -50.597f, 0);
    }


    void handleMCQ2()
    {
        mcq2_Clone = Instantiate(MCQUI[1], transform.position, transform.rotation) as GameObject;
        mcq2_Clone.transform.SetParent(GameObject.FindGameObjectWithTag("UI").transform, false);
        mcq2_Clone.transform.rotation = Quaternion.Euler(0, 0, 0);
        mcq2_Clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_xAxis, m_yAxis);
        mcq2_Clone.tag = "MCQ";
        // mcq1_Clone.transform.position = new Vector3(1.6412f, -50.597f, 0);
    }



    void handleMCQ3()
    {
        mcq3_Clone = Instantiate(MCQUI[2], transform.position, transform.rotation) as GameObject;
        mcq3_Clone.transform.SetParent(GameObject.FindGameObjectWithTag("UI").transform, false);
        mcq3_Clone.transform.rotation = Quaternion.Euler(0, 0, 0);
        mcq3_Clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_xAxis, m_yAxis);
        mcq3_Clone.tag = "MCQ";
        // mcq1_Clone.transform.position = new Vector3(1.6412f, -50.597f, 0);
    }
    void handleMCQ4()
    {
        mcq4_Clone = Instantiate(MCQUI[3], transform.position, transform.rotation) as GameObject;
        mcq4_Clone.transform.SetParent(GameObject.FindGameObjectWithTag("UI").transform, false);
        mcq4_Clone.transform.rotation = Quaternion.Euler(0, 0, 0);
        mcq4_Clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_xAxis, m_yAxis);
        mcq4_Clone.tag = "MCQ";
        // mcq1_Clone.transform.position = new Vector3(1.6412f, -50.597f, 0);
    }
    public void destroyMCQ()
    {
        GameObject.FindObjectOfType<NavMeshAgent>().speed = 10;
        Destroy(GameObject.FindGameObjectWithTag("MCQ"));
    }
    #endregion
}
