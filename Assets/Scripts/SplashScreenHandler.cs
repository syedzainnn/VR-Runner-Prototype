﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SplashScreenHandler : MonoBehaviour 
{
public Animator anim;
#region Variables
#endregion

#region Unity Methods

void Start()
{
	StartCoroutine(loadSceneASYNC());
	
}
	#endregion

	IEnumerator loadSceneASYNC()
	{
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("HomeScreen");
		asyncLoad.allowSceneActivation = false;
		yield return new WaitForSeconds(3);
		asyncLoad.allowSceneActivation = true;
	}
}
