﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class SceneHandler : MonoBehaviour
{
    public Animator anim;
    #region Variables
    public GameObject transitionScreen;
	public Slider slider;
	public TextMeshProUGUI progressText;
    #endregion

    #region Unity Methods

    void Start()
    {
        transitionScreen.SetActive(false);
    }
    #endregion
    public void GameScene()
    {
		transitionScreen.SetActive (true);
        anim.SetTrigger("Fade_Out");
        StartCoroutine(loadSceneASYNC());
        // SceneManager.LoadScene("Scene2");

    }
    IEnumerator loadSceneASYNC()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Scene2");
        while (!asyncLoad.isDone)
        {
            float progress = Mathf.Clamp01(asyncLoad.progress / .9f);
            slider.value = progress;
			progressText.text = progress * 100f + " " + "%";
			
            yield return null;
        }
        // asyncLoad.allowSceneActivation = false;
        // yield return new WaitForSeconds(0.1f);
        // asyncLoad.allowSceneActivation = true;

    }

}
