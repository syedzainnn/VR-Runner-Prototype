﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
public class ScoreManager : MonoBehaviour
{
    float timer = 00.00f;
    float score;
    #region Variables
    public bool isPlaying = false;
    #endregion
    // public Text timerText;
    private TextMeshProUGUI timerText;
    public TextMeshProUGUI scoreText;
    public GameObject GameoverUI;
    public GameObject finalScore;
    public GameObject HomeBUtton;
    public int Selection;
    public Animator anim;
    public AudioSource ErrorSound, SuccessSound;
    #region Unity Methods

    void Start()
    {
        timerText = GetComponent<TextMeshProUGUI>();
        timerText.SetText("00.00");
        scoreText.SetText("Score: 0");
        GameoverUI.SetActive(false);
        finalScore.SetActive(false);
        HomeBUtton.SetActive(false);
       
    }
public void SetBool()
{
    isPlaying = true;
}
    void Update()
    {
        if(isPlaying == true)
        {
        timer += Time.deltaTime;
        timerText.SetText("Timer: "+ " " + timer.ToString("F2"));
        score = Mathf.Round(((100 - timer) * 100));
        scoreText.SetText("Score:" + " " + score.ToString("F0"));
        if(score < 2000)
        {
            scoreText.color = new Color32(255, 0, 0, 255);
        }
        if(score <= 1)
        {
            isPlaying = false;
            SetBoolFalse();
        }
        }
    }
    public void penalty()
    {
        if(Selection == 0)
        {
        anim.SetTrigger("Penalty");
        timer += 10;
        ErrorSound.Play();
        }
        else if (Selection == 1)
        {
            SuccessSound.Play();
        }
 
    }
    public void SetBoolFalse()
    {
        isPlaying = false;
        GameoverUI.SetActive(true);
        finalScore.SetActive(true);
        HomeBUtton.SetActive(true);
    }
	public void GameOver()
	{
		Time.timeScale = 0;    
        isPlaying  = false;
	}
public void WrongSelection()
{
    Selection = 0;
    Debug.Log(Selection);
}
public void CorrectSelection()
{
    Selection = 1;
    Debug.Log(Selection);
}
    #endregion
}
